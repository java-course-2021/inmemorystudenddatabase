/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.hiik.studentdatabase;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import java.io.File;
import java.io.IOException;
import java.time.LocalDate;
import java.time.Month;
import ru.hiik.studentcore.Address;
import ru.hiik.studentcore.Person;
import ru.hiik.studentcore.Professor;
import ru.hiik.studentcore.Student;

/** 
 * Стартовый класс программы БАЗА ДАННЫХ СТУДЕНТОВ
 * @author developer
 */
public class Start
{

    public static void main(String[] args) throws IOException {
        
        System.out.print("БАЗА ДАННЫХ СТУДЕНТОВ ");
        System.out.println("версия 0.0.1");
        
        InMemoryStudentDB studentDB = new InMemoryStudentDB();
        studentDB.init();
        
        
        // Формирование списка студентов
        Student student2 = new  Student();
        student2.sureName= "Юн";
        student2.fistName = "Богдан";
        student2.middleName = "Евгеньевич";
        student2.birthDate = LocalDate.of(2005, Month.FEBRUARY, 5);
        student2.id = 2L;
        student2.group= "ССиСК";
        student2.yearOfStudy=5;
        
        Student student1 = new  Student();
        student1.birthDate =  LocalDate.of(2002, Month.JUNE, 21);
        student1.fistName= "Денис";
        student1.middleName= "Андреевич";
        student1.sureName= "Петков";
        student1.group = "ПКС-420";
        student1.yearOfStudy = 4;
        student1.id = 5L;
        
        // добавление стунтов в списко 
        studentDB.addStudentToMemoryList(student2);
        studentDB.addStudentToMemoryList(student1);
        studentDB.addStudentToMemoryList(student1);
        
        // Печать списка студентов
        studentDB.printAllStudentsFromMemory();
        studentDB.addYarOfStudy();
        
        
        Address address1 = new Address();
        address1.id = 5;
        address1.personId = 2;
          
        String str = student2.toString();
        
        Professor p = new Professor();
        p.id = 1;
        p.fistName ="Дмитрий";
        p.sureName = "Ваганов";
        p.middleName = "Валерьевич";
        p.birthDate = LocalDate.of(1968, Month.FEBRUARY, 26);
        p.department = "Кафедра информационных технологий";
        p.position ="Старший преподаватель";
        
        
        //System.out.println(""+str);  
        //System.out.println(""+p.toString());  
        
        
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.registerModule(new JavaTimeModule()).configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false); 
        
        //objectMapper.writeValue(new File("target/student.json"), Un);
        String studenJson = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString (student2);
        System.out.println("Преобразование Student --> JSON:");  
        System.out.println(studenJson);  
      
        
        // После передачи через Internet
        Student st = objectMapper.readValue(studenJson, Student.class);	
        System.out.println("Преобразование  Student -->:JSON");  
        System.out.println("На сервере студентов: "+st.toString());  
        
    }
    
}
