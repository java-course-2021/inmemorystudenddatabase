/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.hiik.studentdatabase;

import java.util.ArrayList;
import java.util.List;
import ru.hiik.studentcore.Professor;
import ru.hiik.studentcore.Student;

/**
 *
 * @author developer
 */
public class InMemoryStudentDB {
    
   List <Student> students = new ArrayList(); 
   List <Professor> professors = new ArrayList(); 
       
   /**
    * Печать на экран полного списка студентов
    */
   public void printAllStudentsFromMemory()
   {
       System.out.println("Список студентов:");
       int index=0;
       
       for (Student student : students) {
           index = index +1;
           System.out.println("["+index+"] "+student);
           
       }
       
   }
   
   
    /**
    * Печать на экран полного списка студентов
    */
   public void addYarOfStudy()
   {
       System.out.println("Список студентов:");
       int index=0;
       
       for (Student student : students) {
           
           index++;
           int old = student.yearOfStudy;
           int newYear = old+1;
           
           System.out.println("["+index+"] "+student +"Переведен на курс :"+newYear);
           student.yearOfStudy = newYear;
           
       }
       
   }
   
   
   
   
   public void addStudentToMemoryList(Student student)
   {
       students.add(student);
       System.out.println("Добавлен в базу студент: "+student);
       int size = students.size();
       System.out.println("Количество студентов в базе: "+size);
   }
   
   
    
   public void init()
   {
       System.out.println("Инициализирована БД студентов в пямяти");
       int size = students.size();
       System.out.println("Количество студентов в базе: "+size);
   }        
 
   /**
    *  Метод добавляет студента в файл студентов формата JSON
    * 
    */
    
   
   public void putStudentToJsonDatabase()
   {
       
       
   }

   public void deleteStudentFromJsonDatabase()
   {
       
       
   }
   
}
